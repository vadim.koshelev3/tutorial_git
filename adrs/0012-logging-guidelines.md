# 12. Logging guidelines

Date: 2022-08-17

## Logging parameters

Be sure to include IDs and important request parameters into your logging statements for traceability. 
If the request model has a probability of being large, you should limit the amount collection/dynamic parts being dumped into the log. 

The support logger should always log enough metadata so that it's clear in what context the log message was logged. 

Examples of useful values for that context are:

 * **Request ID**
 * **Product ID**
 * **Saleable unit ID**
 * **Journey ID**
 * **Stay period (start date + duration)**
 * **Participant age(s)**
 * **Board ID**
 * **X-Trace-Id**

(To allow for that to happen we need this logger to integrate with the reactive context.)

## Logging places

Be sure to have a `debug` level logging entry point and an `info` logging entry point to all major services, 
to have a transparent view on workflow in form of a log history.

Common technical parts that have high invocation rate like repositories should not contain high level logging (`info`, `debug`). 
If it is absolutely necessary, use `trace` level and manually disable/enable the logging level by adjusting the logging properties on a per-investigation basis.


Be wary of the surrounding method calls and their entry/exit logging points to minimize log spamming. 
For example, is you have a function that logs it's processing per element and the callers are mostly using it in a loop, externalize logging to specific call places to avoid spamming.

## Collections limiting

If you are logging a data model or a collection directly - always consider the possible size of the structure you are going to dump into the log.
If it is possible that it will be larger than **99** elements, be sure to cut down the actually dumped elements.
Also make sure to print how much elements are displayed and what is the total count of elements like `(showing only 99 out of 700)`

Simple example: `collection.take(99)`.

## Frequency of logging

Be wary of logging inside loops, **onNext** hooks or any other frequently repeating piece of code.

In case of loops - try to move the logging before and after the looping 
with a message that generalizes the inputs/outputs of the loop in question.

In case of streams - try to use logs closer to collections points, or if it is absolutely necessary
to get near realtime batch style logging inside a [window()](https://projectreactor.io/docs/core/release/reference/#_windowing_with_fluxfluxt).

## Logging format
```
logger.debug("MESSAGE: firstParameterKey={}, secondParameterKey={}", firstParameterValue, secondParameterValue)
```

Format should be consistent and easy to parse/index if needed. A message followed by the parameters in key/value format.

## Logging eagerness

Logging can be resource consuming, to be able to switch off unnecessary processing please use guards.

If you are using string interpolation or string builders for logging statements be sure to use lazy logging trough lambdas or `if isLoggingLevelEnabled()` guards.

Example of a guard:
```kotlin
if (logger.isErrorEnabled) {
    logger.error("Error parsing aggregate with id ${row["id"]} from $tableName: ${e.localizedMessage}", e)
}
```

Example of a lazy lambda:
```kotlin
logger.error(e) { "Error parsing aggregate with id ${row["id"]} from $tableName: ${e.localizedMessage}" }
```

(Second solution might require additional dependencies https://github.com/MicroUtils/kotlin-logging)

## Logging levels purpose
* **TRACE** - while  **DEBUG** and **INFO** level should be restricted by amount of logging, trace levels should be only turned on by specific logging level on short periods of time to have the most possible data and insight in the process, so even though you rarely will use it in development, be sure to keep in mind the possible issues and what can help track them, and put them behind a guard into a **TRACE** level log.
* **DEBUG** - information that allows us to have insight of what is going on by logging additional details of workflow, should be moderate in terms of frequency and amount of data logged.
* **INFO** - general information about work being done, or initialization, should be very lean in terms of frequency and amount of data logged.
* **WARN** - situational awareness level, that can indicate something that might impact the way everything work, for example if required configuration was defaulted to some value, or something that should not have happened, happened but the code successfully recovered from it. Or unusual data situation when we expected to data be always available, but it was not.
* **ERROR** - Exceptions, argument errors anything that is not part of the workflow, but excluding issues of business workflow, for example there should not be an error log if request had no offers, since we see clearly.
 
Tricky place to decide which level to use might be an exception catching/retry. The most practical approach would be to log the retries as info levels and if they will not succeed, then propagate it to the error level with details why the attempt failed.

## Logging levels cohesion

Be wary that not every environment will have `debug` level turned on.
When placing logging, please run tests and validate by eye that logging with only `info` level still makes sense and can explain the workflow without resorting to enabling `debug` level

## Sensitive parameters

Do not log keys, usernames, passwords, etc, without masking.


## Additional

Be sure to read additional recommendations that are stored and updated on [confluence](https://confluence.tuigroup.com/pages/viewpage.action?pageId=1109244072#Price&Availability:Errors,LoggingandTracing-Logging)
