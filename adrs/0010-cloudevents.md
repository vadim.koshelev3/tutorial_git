# 10. CloudEvents

Date: 2022-02-14

## Status

Accepted

## Context

[CloudEvents](https://cloudevents.io/) have been chosen as the standard format for events inside of TUI. TUI expands on
this standard with some [custom attributes](https://confluence.tuigroup.com/display/TEDP/CloudEvents+TUI+Business+Extension).

NOTE: These fields are *LOWER CASE ONLY*, and *NOT* camelCase!


## Decision

The P&A project embraces this standard and its extension and will focus on it both for internal AND external events.

## Consequences

* We will enforce good event practices automatically inside of our service. Events will get an explicit type, tracing
  using the business process name and ID is built-in. Deserialization from 'mixed' sources becomes more structured
  since the type field is required.
* We will need to migrate away from our current 'simple' events towards this standard. This will happen using the
  'boyscout principle': whenever we need to change one of the 'old' events we will bump it to an equivalent cloud-event.
* When we would mix events into a single 'bucket', we will still be able to easily identify the type of each event.
