SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
echo "###### Running Test"
for filename in src/*.dsl; do
    if  docker run -it --rm -v ${SCRIPTPATH}:/usr/local/structurizr structurizr/cli export -format json -w $filename   ; then
        echo ""
    else
        echo ""
        echo ""
        echo "###### Test Failed for ${filename}"
        exit 1
    fi
done
echo "###### Test Completed Successfully"