productAggregate = container ProductAggregate {
    description "Product Aggregate"
    tags "aggregate,DDD,product"

    product = component Product {
        tags "entity,DDD,product"
    }
    productComponent = component Component {
        tags "entity,DDD,product"
    }
    productUnit = component Unit {
        tags "entity,DDD,product"
    }

    accommodationComponent = component AccommodationComponent {
        tags "entity,DDD,product"
    }
    flightComponent = component FlightComponent {
        tags "entity,DDD,product"
    }


    accommodationUnit = component AccommodationUnit {
        tags "entity,DDD,product"
    }
    flightUnit = component FlightUnit {
        tags "entity,DDD,product"
    }


    flightComponent -> productComponent "implements"
    accommodationComponent -> productComponent "implements"

    accommodationUnit -> productUnit "implements"
    flightUnit -> productUnit "implements"
    accommodationUnit -> accommodationComponent "references"
    flightUnit -> flightComponent "references"

    product -> productComponent "references" "" "componentId"
    productUnit -> productComponent "references" "" "componentId"
}
