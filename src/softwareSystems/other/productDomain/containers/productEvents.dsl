productEvents = container ProductEvents {
    productCreated = component ProductCreated {
        description "Event to create product"
        url https://confluence.tuigroup.com/x/ba8KOQ
        tags "event,DDD,product,create"
    }
    productUpdated = component ProductUpdated {
        description "Event to update a product"
        url https://confluence.tuigroup.com/x/ba8KOQ
        tags "event,DDD,product,update"
    }
    productRetired = component ProductRetired {
        description "Event to retire a product"
        url https://confluence.tuigroup.com/x/ba8KOQ
        tags "event,DDD,product,delete"
    }

    flightComponentEvent = component ProductFlightComponentEvent {
        description "Event to create or update an flight component"
        tags "event,DDD,product"
        properties {
            foo "baz"
        }
    }

    accommodationComponentEvent = component ProductAccommodationComponentEvent {
        description "Event to create or update an accommodation component"
        tags "event,DDD,product"
        properties {
            foo "bar"
        }
    }

    flightUnitCreated = component FlightUnitCreatedEvent {
        description "Event to create a flight unit"
        tags "event,DDD,product"
    }
    flightUnitUpdated = component FlightUnitUpdatedEvent {
        description "Event to update a flight unit"
        tags "event,DDD,product"
    }
    flightUnitRetired = component FlightUnitRetiredEvent {
        description "Event to retire a flight unit"
        tags "event,DDD,product"
    }

    accommodationUnitCreated = component AccommodationUnitCreatedEvent {
        description "Event to create an accommodation unit"
        url "https://confluence.tuigroup.com/display/ONEPF/Business+Event%3A+Product+Accommodation+Unit+Event"
        tags "event,DDD,product"
    }

    accommodationUnitUpdated = component AccommodationUnitUpdatedEvent {
        description "Event to create an accommodation unit"
        url "https://confluence.tuigroup.com/display/ONEPF/Business+Event%3A+Product+Accommodation+Unit+Event"
        tags "event,DDD,product"
    }

    accommodationUnitRetired = component AccommodationUnitRetiredEvent {
        description "Event to create an accommodation unit"
        url "https://confluence.tuigroup.com/display/ONEPF/Business+Event%3A+Product+Accommodation+Unit+Event"
        tags "event,DDD,product"
    }
}
