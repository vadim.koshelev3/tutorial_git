productDomain = softwareSystem "Product Domain" {
    tags "${TAG_DOMAIN}"
    
    !include "containers/"

    # Relationships
    productEvents.productCreated -> productAggregate.product "manages" {
        tags "${TAG_DATA_FLOW}"
    }
    productEvents.productUpdated -> productAggregate.product "manages"{
        tags "${TAG_DATA_FLOW}"
    }
    productEvents.productRetired -> productAggregate.product "manages"{
        tags "${TAG_DATA_FLOW}"
    }

    productEvents.flightComponentEvent -> productAggregate.flightComponent "manages"{
        tags "${TAG_DATA_FLOW}"
    }
    productEvents.accommodationComponentEvent -> productAggregate.accommodationComponent "manages"{
        tags "${TAG_DATA_FLOW}"
    }

    productEvents.flightUnitCreated -> productAggregate.flightUnit "manages"{
        tags "${TAG_DATA_FLOW}"
    }
    productEvents.flightUnitUpdated -> productAggregate.flightUnit "manages"{
        tags "${TAG_DATA_FLOW}"
    }
    productEvents.flightUnitRetired -> productAggregate.flightUnit "manages"{
        tags "${TAG_DATA_FLOW}"
    }

    productEvents.accommodationUnitCreated -> productAggregate.accommodationUnit "manages"{
        tags "${TAG_DATA_FLOW}"
    }
    productEvents.accommodationUnitUpdated -> productAggregate.accommodationUnit "manages"{
        tags "${TAG_DATA_FLOW}"
    }
    productEvents.accommodationUnitRetired -> productAggregate.accommodationUnit "manages"{
        tags "${TAG_DATA_FLOW}"
    }

}