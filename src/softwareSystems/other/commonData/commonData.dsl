commonData = softwareSystem "Common Data"{
    tags "${TAG_DOMAIN}"
    
    brand = container Brand {
        description "Hard-coded TUI brands"
    }
    sourceMarket = container SourceMarket {
        description "Hard-coded TUI Source Markets"
    }

    sourceMarket -> brand 
}   