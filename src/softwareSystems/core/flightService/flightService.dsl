flightService = softwareSystem "Flight Service" {
    tags "${TAG_EXTERNAL_SERVICE},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"

    journeyInventoryChangeEventsSNS = container "Journey Inventory Change Events: SNS" {
        tags "${ICON_AWS_SNS},${TAG_JOURNEY_INVENTORY_CHANGE_EVENT}"
    }    
}
