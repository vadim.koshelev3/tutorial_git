commandAppDatabase = container "Command App Database" "" "Aurora" {
    tags "Amazon Web Services - Aurora"

    commandAppSchema = group "Command App Schema" {
        inventoryTranscationTable = component "Inventory Transaction Table" "" "" {
            tags ""
        }   
        productTable = component "Product Table" "" "" {
            tags ""
        } 
        flightTable = component "Flight Table" "" "" {
            tags ""
        } 
    }                   
}