commandApp = container "Command APP" {
    technology "Kotlin"
    tags "${ICON_KOTLIN},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT}"

    #
    # Components
    # 
    domainBusinessLogicInventoryTransacationCreated = component "Domain Business Logic: Inventory Transaction Created Command" "Kotlin" "Kotlin" {
        tags "${ICON_KOTLIN_COMPONENT}, ${TAG_INVENTORY_TRANSACTION_CREATED_COMMAND},${TAG_INVENTORY_TRANSACTION_CREATED_DOMAIN_EVENT}"
        # -> commandAppEventDomainBusQueue "Inventory Transaction Created Domain Event" "" ", ${TAG_INVENTORY_TRANSACTION_CREATED_DOMAIN_EVENT}"
    }

    commandHandlerInventoryTransactionCreatedCommand = component "Command Handler: Inventory Transaction Created Command" "Kotlin" "Kotlin" {
        tags "${ICON_KOTLIN_COMPONENT}, , ${TAG_INVENTORY_TRANSACTION_CREATED_COMMAND}"
        # -> commandAppDomainBusinessLogicInventoryTransacationCreated "" "" ", ${TAG_INVENTORY_TRANSACTION_CREATED_COMMAND}"
    }

    commandHandlerJourneyInventoryChangeCommand = component "Command Handler: Journey Inventory Change Command" "Kotlin" "Kotlin" {
        tags "${ICON_KOTLIN_COMPONENT}"
        # -> commandAppStoreDatabase "" "" ""
    }

    commandBus = component "Command Bus" "Kotlin" "Kotlin" {
        tags "${ICON_KOTLIN_COMPONENT},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT}"
        # -> commandAppCommandHandlerInventoryTransactionCreatedCommand "Inventory Transaction Created Command" "" "${TAG_INVENTORY_TRANSACTION_CREATED_COMMAND}"
        # -> commandAppCommandHandlerJourneyInventoryChangeCommand "Journey Inventory Change Command" "" ""
    }

    eventHandlerInventoryTransactionCreated = component "Event Handler: Inventory Transaction Created" "Kotlin" "Kotlin" {
        tags "${ICON_KOTLIN_COMPONENT}, , ${TAG_INVENTORY_TRANSACTION_CREATED_SPLITT}, ${TAG_INVENTORY_TRANSACTION_CREATED_COMMAND}"
        # -> commandAppCommandBus "Inventory Transaction Created Command" "" ",${TAG_INVENTORY_TRANSACTION_CREATED_COMMAND}"
    }

    eventHandlerJourneyInventoryChange = component "Event Handler: Journey Inventory Change" "Kotlin" "Kotlin" {
        tags "${ICON_KOTLIN_COMPONENT},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT}"
        # -> commandAppCommandBus " Journey Inventory Change Command" "" ""
    }  

    eventListener = component "Event Listener" "Kotlin" "Kotlin" {
        tags "${ICON_KOTLIN_COMPONENT},${TAG_INVENTORY_TRANSACTION_CREATED_EVENT_SPLIT}"
        # -> commandApptEventHandlerInventoryTransactionCreatedSplitt "Inventory Transaction Created Split Event" "" ", ${TAG_INVENTORY_TRANSACTION_CREATED_SPLITT}"
        # -> commandApptEventHandlerJourneyInventoryChange "Journey Inventory Change Event" "" ""
    }

    #
    # Relationships: Component Data Flow
    # 

    # Inventory Transaction Created 
    eventListener -> eventHandlerInventoryTransactionCreated "" {
        tags "${TAG_DATA_FLOW}"
    }
    eventHandlerInventoryTransactionCreated -> commandBus "" {
        tags "${TAG_DATA_FLOW}"
    }
    commandBus -> commandHandlerInventoryTransactionCreatedCommand "" {
        tags "${TAG_DATA_FLOW}"
    }
    commandHandlerInventoryTransactionCreatedCommand -> domainBusinessLogicInventoryTransacationCreated "" {
        tags "${TAG_DATA_FLOW}"
    }

    # Journey Inventory Change 
    eventListener -> eventHandlerJourneyInventoryChange "" {
        tags "${TAG_DATA_FLOW}"
    }
    eventHandlerJourneyInventoryChange -> commandBus "" {
        tags "${TAG_DATA_FLOW}"
    }
    commandBus -> commandHandlerJourneyInventoryChangeCommand "" {
        tags "${TAG_DATA_FLOW}"
    }

}
