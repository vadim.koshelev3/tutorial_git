workspace {
    name "Domain Arcitecture" 

    !identifiers hierarchical

    model {
        !include "models/domain.dsl
    }

    views {
        !include "views/"
    }
}
