styles {
    
    element "${GROUP_PANDA_WHOLESALE_SERVICE}" {
        background #1168bd
        color #ffffff
        stroke #ffffff
    }

    element "Person" {
        shape Person
    }
    element "Software System" {
        background #1168bd
        color #ffffff
    }
    element "External Service" {
        background #999999
        color #ffffff
    }
    element "Domain" {
        background #313030
        color #ffffff
    }
    element "Kotlin" {
        shape RoundedBox
        background #1168bd
        color #ffffff
    }
    relationship "${TAG_RELATIONSHIP_USES}" {
        #routing Curved
        colour #00008b
        style solid
    }
    relationship "${TAG_RELATIONSHIP_DATA_FLOW}" {
        routing Curved
        colour #0bb1fb
        # style solid
    }

    relationship "${TAG_RELATIONSHIP_DATA_FLOW_SYNC}" {
        routing Curved
        colour #0bb1fb
        style solid
    }
    
    relationship "${TAG_RELATIONSHIP_DATA_FLOW_ASYNC}" {
        routing Curved
        colour #0bb1fb
        # style solid
    }
}