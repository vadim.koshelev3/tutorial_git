!include constants/core.dsl

workspace {
    name "E2E Arcitecture"
    !identifiers hierarchical
    # !impliedRelationships false
    
    !adrs ../adrs
    
    model {
        !include models/core.dsl
    }
    
    views {
        !include views/
    }
}
