SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
docker run -it --rm -p 8081:8080 -v ${SCRIPTPATH}:/usr/local/structurizr structurizr/lite 